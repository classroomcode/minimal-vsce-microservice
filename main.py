from flask import Flask, request, abort

server = Flask(__name__)

@server.route("/")
def index():
    return ""

@server.route("/world")
def hello(method=["GET"]):
    data = request.get_json()
    resolve, failure = validate_data(data)
    method, params = data["method"], data["params"]
    if method == "hello":
        return resolve(f"hello, world!")
    else:
        return failure(1, f"Undefined reference to '{method}'")

def validate_data(data):
    if ("jsonrpc" not in data 
        or data["jsonrpc"] != "2.0"
        or "method" not in data
        or "params" not in data
    ):
        abort(400)
    
    if "id" not in data: # Response not expected as this is a notification
        return lambda x: "", lambda *args: ""
    
    def resolve(result):
        return {
            "jsonrpc": data["jsonrpc"],
            "id": data["id"],
            "result": result
        }
    
    def failure(code: int, message: str, result=None):
        return {
            "jsonrpc": data["jsonrpc"],
            "id": data["id"],
            "error": {
                "code": code,
                "message": message,
                "data": result,
            }
        }
    
    return resolve, failure

if __name__ == "__main__":
    import argparse
    import subprocess
    import os

    parser = argparse.ArgumentParser()
    parser.add_argument("host")
    parser.add_argument("port")
    args = parser.parse_args()

    os.environ["FLASK_APP"] = __file__
    
    subprocess.call(["flask", "run", "-h", args.host, "-p", args.port])