import requests
import unittest
import subprocess
import signal

class Test_Server(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.process = subprocess.Popen(
            ["python3", "main.py", "localhost", "8000"],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )

        while True: # Wait for process to finish init
            try:
                requests.request(
                    "GET",
                    "http://localhost:8000"
                )
            except:
                continue
            else:
                break
    
    @classmethod
    def tearDownClass(cls):
        cls.process.send_signal(signal.SIGINT)
        cls.process.wait()

    def test_helloworld(self):
        req = requests.request(
            "GET",
            "http://localhost:8000/world",
            json={"jsonrpc": "2.0", "method": "hello", "params":{}, "id":0},
        )
        self.assertEqual(req.status_code, 200)
        self.assertEqual(req.json()["result"], "hello, world!")
    
    def test_helloworld_error(self):
        req = requests.request(
            "GET",
            "http://localhost:8000/world",
            json={"jsonrpc": "2.0", "method": "world", "params":{}, "id": 0},
        )
        self.assertEqual(req.status_code, 200)
        response = req.json()
        self.assertIn("error", response)
        error_object = response["error"]
        self.assertIn("code", error_object)
        self.assertIn("message", error_object)
    
    def test_helloworld_notify(self):
        req = requests.request(
            "GET",
            "http://localhost:8000/world",
            json={"jsonrpc": "2.0", "method": "hello", "params": {}}
        )
        self.assertEqual(req.status_code, 200)

if __name__ == "__main__":
    unittest.main()